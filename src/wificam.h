#ifndef WIFICAM_H
#define WIFICAM_H

#include <QStringList>
#include <QtXml>
#include <QXmlStreamReader>

extern "C"{
#include "wifiecam_apis.h"
}


class WifiCam:public QObject{
     Q_OBJECT
public:
    WifiCam();
    ~WifiCam();

    // methods
    bool getDefaultInterfaceName();

    bool openConnection();

    QStringList extractWifiCamsFromAPList();

    void closeConnection();

    bool getStatus(QString deviceName);


    enum camSettings{
        VIDEO_RESOLUTION = 0,
        VIDEO_POWERLINEFREQ,
        VIDEO_FLIP,
        VIDEO_MIRROR,
        ENCODE_STREAM1_TYPE,
        ENCODE_STREAM1_FRAMESIZE,
        ENCODE_STREAM1_FRAMERATE,
        ENCODE_STREAM1_INTRA_FRAME_PERIOD,
        ENCODE_STREAM1_VIDEO_RATECTRL_MODE,
        ENCODE_STREAM1_VIDEO_BITRATE,
        ENCODE_STREAM1_VIDEO_QUALITY_H264_MPEG4,
        ENCODE_STREAM1_VIDEO_QUALITY_MJPEG,
        ENCODE_STREAM1_TEXT_OVERLAY,
        ENCODE_STREAM1_TEXT_FIELD,
        ENCODE_STREAM2_TYPE,
        ENCODE_STREAM2_FRAMESIZE,
        ENCODE_STREAM2_FRAMERATE,
        ENCODE_STREAM2_INTRA_FRAME_PERIOD,
        ENCODE_STREAM2_VIDEO_RATECTRL_MODE,
        ENCODE_STREAM2_VIDEO_BITRATE,
        ENCODE_STREAM2_VIDEO_QUALITY_H264_MPEG4,
        ENCODE_STREAM2_VIDEO_QUALITY_MJPEG,
        ENCODE_STREAM2_TEXT_OVERLAY,
        ENCODE_STREAM2_TEXT_FIELD,
        ENCODE_STREAM3_TYPE,
        ENCODE_STREAM3_FRAMESIZE,
        ENCODE_STREAM3_FRAMERATE,
        ENCODE_STREAM3_INTRA_FRAME_PERIOD,
        ENCODE_STREAM3_VIDEO_RATECTRL_MODE,
        ENCODE_STREAM3_VIDEO_BITRATE,
        ENCODE_STREAM3_VIDEO_QUALITY_H264_MPEG4,
        ENCODE_STREAM3_VIDEO_QUALITY_MJPEG,
        ENCODE_STREAM3_TEXT_OVERLAY,
        ENCODE_STREAM3_TEXT_FIELD,
        AUDIO_MUTE,
        AUDIO_TYPE,
        AUDIO_BITRATE_G711_BR,
        AUDIO_BITRATE_G726_BR,
        AUDIO_BITRATE_AAC_BR,
        AUDIO_BITRATE_GSM_AMR,
        ISP_EXPOSURE_AUTO_EXPOSURE_MODE,
        ISP_EXPOSURE_EXPOSURECUSTOMIZED,
        ISP_EXPOSURE_EXPOSUREGRID,
        ISP_EXPOSURE_TARGET_LUMINANCE,
        ISP_EXPOSURE_OFFSET,
        ISP_EXPOSURE_GAIN,
        ISP_EXPOSURE_SHUTTER_SPEED,
        ISP_WDR_LEVEL,
        ISP_WDR_CUSTOM_LEVEL,
        ISP_WB_WINDOW,
        ISP_WB_CUSTOMIZED,
        ISP_WB_GRID,
        ISP_WB_MODE,
        ISP_WB_RED_GAIN,
        ISP_WB_BLUE_GAIN,
        ISP_IA_BRIGHTNESS,
        ISP_IA_SATURATION,
        ISP_IA_CONTRAST,
        ISP_NR_2D3DNR,
        ISP_NR_2D3DNR_MODE,
        ISP_NR_NORMAL_STRENGTH,
        ISP_NR_MOTION_ADAPTIVE,
        ISP_NR_CURRENTWEIGHT,
        ISP_NR_REF_STRENGTH,
        ISP_NR_DEIMPULSE,
        ISP_NR_SENSITIVITY,
        ISP_EDGE_ENHANCE,
        ISP_EDGE_EDGE_SENSITIVE,
        ISP_EDGE_NOISE_SENSITIVE,
        ISP_EDGE_EDGE_STRENGTH,
        ISP_EDGE_EDGE_STRENGTH_CLIP,
        CAMERA_CONFIG_END
    };

    struct cameraConfigValStruct{
        QString fieldName;
        QString support;
        QString defaultValue;
        QString currentValue;
    };
    struct cameraConfigValStruct camConfigVal[CAMERA_CONFIG_END];

signals:
    void deviceStatus(QString title, QString message);

public slots:
    bool socketInit(QString ifname, QString ipaddr);
    void socketClose();
    void camQueryCapability();
    void parseCameraConfigXmlFile();

private:
    struct ssid_struct *m_ssidList;
    char *m_ctrlInterfaceName;
    HANDLE m_wifiCamhandle;
    unsigned char *m_camCapability;
    QXmlStreamReader xml;
    QFile xmlFile;

    void saveCameraConfigXmlFile();
    void openCameraConfigXmlFile();
    void parseVideoSettingElements();
};

#endif // WIFICAM_H
