import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.0
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1
import econ.camera.wificam 1.0

Item {
    ColumnLayout{
        id: wifiCamColumnLayout
        Item{
            id: wifiInterfaceConfig
            Button{
                id:wifiIfaceConfigButton
                y:206
                opacity: 1
                tooltip: "Wifi Camera Interface Configuration - User can set wifi interface name, ip address, rtsp streaming url"
                activeFocusOnPress : true
                style: ButtonStyle {
                    background: Rectangle {
                        implicitWidth: 265
                        implicitHeight: 15
                        border.width: control.activeFocus ? 1 : 0
                        color: control.activeFocus ? "#df643e" : wifiIfaceConfigScroll.visible ? "#df643e" : "#222021"  ///*#df643e"//*/
                        border.color: control.activeFocus ? "#df643e" : "#222021"
                    }
                    label: Image {
                        horizontalAlignment: Image.AlignLeft
                        fillMode: Image.PreserveAspectFit
                        source: "images/videocapturesettings.png"       // sankari: To do: need to modified the image
                    }
                }
                onClicked: {
                    wifiInterfaceConfiguration()
                }

                onFocusChanged: {
                    wifiIfaceConfigScroll.visible = false
                }
                Keys.onSpacePressed: {

                }
                Keys.onReturnPressed: {                    
                    wifiInterfaceConfiguration()
                }

                ScrollView {
                    id: wifiIfaceConfigScroll
                    x:10
                    y: 35
                    width: 257
                    height: 150
                    visible: false
                    style: ScrollViewStyle {
                        scrollToClickedPosition: true
                        handle: Image {
                            id: wifiIfaceConfigScrollhandle
                            source: "images/scroller.png"
                        }
                        scrollBarBackground: Image {
                            id: wifiIfaceConfigScrollStyle
                            source: "images/Scroller_bg.png"
                        }
                        incrementControl: Image {
                            id: wifiIfaceConfigIncrement
                            source: "images/down_arrow.png"
                        }
                        decrementControl: Image {
                            id: wifiIfaceConfigDecrement
                            source: "images/up_arrow.png"
                        }
                    }
                    Item{
                        id:wifiIfaceConfigItem
                        height:connectWifiCam.y+connectWifiCam.height+50
                        GridLayout {
                            columns: 2
                            rowSpacing: 15
                            Text {
                                id: ifname
                                text: "Interface Name"
                                font.pixelSize: 14
                                font.family: "Ubuntu"
                                color: "#ffffff"
                                smooth: true
                                opacity: 1
                            }
                            TextField {
                                id: ifNameTextbox
                                font.pixelSize: 10
                                font.family: "Ubuntu"
                                smooth: true
                                horizontalAlignment: TextInput.AlignHCenter
                                style: econTextFieldStyle
                                //validator: RegExpValidator { regExp: "/wlan[0-9]/" }
                                implicitWidth: 140
                                onTextChanged: {

                                }
                            }
                            Text {
                                id: ipaddr
                                text: "IP Address"
                                font.pixelSize: 14
                                font.family: "Ubuntu"
                                color: "#ffffff"
                                smooth: true
                                opacity: 1
                            }
                            TextField {
                                id: ipaddrTextbox
                                font.pixelSize: 10
                                font.family: "Ubuntu"
                                smooth: true
                                horizontalAlignment: TextInput.AlignHCenter
                                style: econTextFieldStyle
                                //  validator: RegExpValidator { regExp: "^\d+\.\d+\.\d+\.\d+$" }
                                implicitWidth: 140
                                onTextChanged: {
                                    // wificamera.socketInit(ifNameTextbox.text.toString(), ipaddrTextbox.text.toString())
                                }
                            }

                            Button {
                                id: connectWifiCam
                                opacity: 1
                                enabled: true
                                activeFocusOnPress : true
                                text: "Connect"
                                style: econButtonStyle
                                tooltip: "Click to connect the wifi cam"
                                onClicked: {
                                    if(wificamera.socketInit(ifNameTextbox.text.toString(), ipaddrTextbox.text.toString())){
                                        wificamera.camQueryCapability();

                                    }
                                }

                                Keys.onReturnPressed: {
                                    if(wificamera.socketInit(ifNameTextbox.text.toString(), ipaddrTextbox.text.toString())){
                                        wificamera.camQueryCapability();

                                    }
                                }
                            }
                            //                            Text {
                            //                                id: rtspUrl
                            //                                text: "Streaming URL"
                            //                                font.pixelSize: 14
                            //                                font.family: "Ubuntu"
                            //                                color: "#ffffff"
                            //                                smooth: true
                            //                                opacity: 1
                            //                            }
                            //                            TextField {
                            //                                id: rtspUrlTextbox
                            //                                font.pixelSize: 10
                            //                                font.family: "Ubuntu"
                            //                                smooth: true
                            //                                horizontalAlignment: TextInput.AlignHCenter
                            //                                style: econTextFieldStyle
                            //                                implicitWidth: 140
                            //                                onTextChanged: {

                            //                                }
                            //                            }
                        }
                    }
                }
            }
        }
        Item{
            id: wifiCameraConfig
            Button{
                id:wifiCameraConfigButton
                y: wifiIfaceConfigItem.visible ? wifiIfaceConfigButton.y + wifiIfaceConfigItem.height : wifiIfaceConfigButton.y + 35
                opacity: 1
                tooltip: "Wifi Camera Interface Configuration - User can set camera settings"
                activeFocusOnPress : true
                style: ButtonStyle {
                    background: Rectangle {
                        implicitWidth: 265
                        implicitHeight: 15
                        border.width: control.activeFocus ? 1 : 0
                        color: control.activeFocus ? "#df643e" : wifiCameraConfigScroll.visible ? "#df643e" : "#222021"  ///*#df643e"//*/
                        border.color: control.activeFocus ? "#df643e" : "#222021"
                    }
                    label: Image {
                        horizontalAlignment: Image.AlignLeft
                        fillMode: Image.PreserveAspectFit
                        source: "images/stillcapturesettings.png"       // sankari: To do: need to modified the image
                    }
                }
                onClicked: {
                    wifiCameraConfiguration()
                    wificamera.parseCameraConfigXmlFile();
                }

                onFocusChanged: {
                    wifiCameraConfigScroll.visible = false
                }
                Keys.onSpacePressed: {

                }
                Keys.onReturnPressed: {
                    console.log("wifi camera config:return key pressed")
                    wifiCameraConfiguration()
                    wificamera.parseCameraConfigXmlFile();
                }
                ScrollView {
                    id: wifiCameraConfigScroll
                    x: 10
                    y: 35
                    width: 257
                    height: 360
                    visible: false
                    style: ScrollViewStyle {
                        scrollToClickedPosition: true
                        handle: Image {
                            id: wifiCameraConfigScrollhandle
                            source: "images/scroller.png"
                        }
                        scrollBarBackground: Image {
                            id: wifiCameraConfigScrollStyle
                            source: "images/Scroller_bg.png"
                        }
                        incrementControl: Image {
                            id: wifiCameraConfigIncrement
                            source: "images/down_arrow.png"
                        }
                        decrementControl: Image {
                            id: wifiCameraConfigDecrement
                            source: "images/up_arrow.png"
                        }
                    }

                    Item{
                        height: 3200
                        ColumnLayout {
                            spacing: 20
                            Row{
                                Text {
                                    id: videoSettings
                                    text: "                    --- Video Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: resolution
                                    text: "Resolution"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                ComboBox {
                                    id: resolutionCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxx"  }
                                        ListElement { text: "yyy" }
                                        ListElement { text: "zzz" }
                                        ListElement { text: "www" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: resolutionSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: resolutionSize.sourceSize.width - 25
                                                height: resolutionSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }

                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: powerLineFrequency
                                    text: "Power Line\nFrequency"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                ExclusiveGroup { id: pwrLineFreqRadio }
                                RadioButton {
                                    exclusiveGroup: pwrLineFreqRadio
                                    id: pwrLineFreq50Hz
                                    text: "50Hz"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }
                                RadioButton {
                                    exclusiveGroup: pwrLineFreqRadio
                                    id: pwrLineFreq60Hz
                                    text: "60Hz"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: colorMode
                                    text: "ColorMode"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                ExclusiveGroup { id: colorModeRadio }
                                RadioButton {
                                    exclusiveGroup: pwrLineFreqRadio
                                    id: colorModeBW
                                    text: "BW"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }
                                RadioButton {
                                    exclusiveGroup: pwrLineFreqRadio
                                    id: colorModeColor
                                    text: "Color"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }

                            }
                            Row{
                                //spacing: 40
                                CheckBox {
                                    id: flipCheck
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: flipCheckText
                                            text: "Flip"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                        }
                                    }
                                }
                            }
                            Row{

                                CheckBox {
                                    id: mirrorCheck
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: mirrorCheckText
                                            text: "Mirror"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                        }
                                    }
                                }
                            }
                            Row{
                                CheckBox {
                                    id: ircutheck
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: irCutCheckText
                                            text: "IR Cut"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                        }
                                    }
                                }
                            }
                            Row{
                                Text {
                                    id: encodeSettings
                                    text: "                    --- Encode Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: encodingText
                                    text: "Encoding"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: encodingCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "H264"  }
                                        ListElement { text: "MPEG4" }
                                        ListElement { text: "MJPEG" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: encodeComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: encodeComboSize.sourceSize.width - 25
                                                height: encodeComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: framesizeText
                                    width:70
                                    text: "Frame Size"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: framesizeCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxxx"  }
                                        ListElement { text: "yyyy" }
                                        ListElement { text: "zzzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: framesizeComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: framesizeComboSize.sourceSize.width - 25
                                                height: framesizeComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: framerateText
                                    text: "Frame Rate"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: framerateCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxxx"  }
                                        ListElement { text: "yyyy" }
                                        ListElement { text: "zzzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: framerateComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: framerateComboSize.sourceSize.width - 25
                                                height: framerateComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: framePeriodText
                                    text: "Intra Frame\nPeriod"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: framePeriodCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxxx"  }
                                        ListElement { text: "yyyy" }
                                        ListElement { text: "zzzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: framePeriodComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: framePeriodComboSize.sourceSize.width - 25
                                                height: framePeriodComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Grid{
                                columns:3
                                rowSpacing: 10
                                spacing: 15

                                Text {
                                    id: rateControl
                                    text: "Rate Control\nMode"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                ExclusiveGroup { id: rateControlModeRadio }
                                RadioButton {
                                    exclusiveGroup: rateControlModeRadio
                                    id: rateControlModeNone
                                    text: "None"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }
                                RadioButton {
                                    exclusiveGroup: rateControlModeRadio
                                    id: rateControlModeVqcb
                                    text: "vqcb"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }
                                Text {
                                    text: "Rate Control\nMode"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0
                                }
                                RadioButton {
                                    exclusiveGroup: rateControlModeRadio
                                    id: rateControlModeCqcb
                                    text: "cqcb"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }
                                RadioButton {
                                    exclusiveGroup: rateControlModeRadio
                                    id: rateControlModeCvbr
                                    text: "cvbr"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }

                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: videobitrateText
                                    text: "Bit Rate"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: videoBitrateCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxx"  }
                                        ListElement { text: "yyy" }
                                        ListElement { text: "zzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: videoBitrateComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: videoBitrateComboSize.sourceSize.width - 25
                                                height: videoBitrateComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: vidQualityText
                                    text: "Video Quality"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                    wrapMode: Text.WordWrap
                                }

                                ComboBox {
                                    id: vidQualityCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxx"  }
                                        ListElement { text: "yyy" }
                                        ListElement { text: "zzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: vidQualityComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: vidQualityComboSize.sourceSize.width - 25
                                                height: vidQualityComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                CheckBox {
                                    id: textOverlayCheck
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: textOverlayCheckText
                                            text: "Text Overlay"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                            wrapMode: Text.WordWrap
                                            width: 50
                                        }
                                    }
                                }
                                TextField {
                                    id: textOverlayTextField
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    enabled: textOverlayCheck.checked ? true : false
                                    opacity : textOverlayCheck.checked ? 1 : 0
                                    horizontalAlignment: TextInput.AlignHCenter
                                    style: econTextFieldStyle
                                    implicitWidth: 145
                                    onTextChanged: {

                                    }
                                }

                            }
                            Row{
                                Text {
                                    id: audioSettings
                                    text: "                    --- Audio Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: encodeType
                                    text: "Encoding Type"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                    wrapMode: Text.WordWrap
                                }

                                ComboBox {
                                    id: encodeTypeCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxx"  }
                                        ListElement { text: "yyy" }
                                        ListElement { text: "zzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: encodeTypeComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: encodeTypeComboSize.sourceSize.width - 25
                                                height: encodeTypeComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: audioBitrateText
                                    text: "Bit Rate"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: audioBitrateCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxx"  }
                                        ListElement { text: "yyy" }
                                        ListElement { text: "zzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: audioBitrateComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: audioBitrateComboSize.sourceSize.width - 25
                                                height: audioBitrateComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                Text {
                                    id: ispExposureSettings
                                    text: "           --- ISP Exposure Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }
                            }
                            Row{
                                Text {
                                    id: exposureModeText
                                    text: "Auto Exposure Mode"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }
                            Row{
                                spacing:10
                                ExclusiveGroup { id: exposureModeRadio }
                                RadioButton {
                                    exclusiveGroup: exposureModeRadio
                                    id: exposureModeAuto
                                    text: "Auto"
                                    width: 50
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }

                                RadioButton {
                                    exclusiveGroup: exposureModeRadio
                                    id: exposureModeBcklight
                                    text: "Backlight"
                                    width: 80
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }

                                RadioButton {
                                    exclusiveGroup: exposureModeRadio
                                    id: exposureModeCustomized
                                    text: "Customized"
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }

                            }
                            Row{
                                Text {
                                    id: exposureGrid
                                    text: "Region Of Interest:"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }
                            Row{
                                Grid{
                                    rows: 15
                                    columns: 16
                                    Repeater{
                                        model: (15*16)
                                        CheckBox {
                                            id: roicheckboxGrid
                                            style: CheckBoxStyle {
                                                label: Text {
                                                    id: roigrid
                                                    font.pixelSize: 14
                                                    font.family: "Ubuntu"
                                                    color: "#ffffff"
                                                    smooth: true
                                                    opacity: 1
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                            Row{
                                Text {
                                    id: exposureCtrl
                                    text: "Exposure Controls:"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }
                            Row{
                                spacing:10
                                Text {
                                    id: lumninanceText
                                    text: "Target Luminance"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: lumninanceSlider
                                    opacity: enabled ? 1 : 0.1
                                    width: 110
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: lumninanceValue
                                    text: lumninanceSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: lumninanceSlider.minimumValue; top: lumninanceSlider.maximumValue;}
                                    opacity: 1
                                    style: econTextFieldStyle
                                    implicitWidth: 50
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            lumninanceSlider.value = lumninanceValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:10
                                Text {
                                    id: offsetText
                                    text: "Offset"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: offsetSlider
                                    opacity: enabled ? 1 : 0.1
                                    width: 110
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: offsetValue
                                    text: offsetSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: offsetSlider.minimumValue; top: offsetSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 50
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            offsetSlider.value = offsetValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:10
                                Text {
                                    id: gainText
                                    text: "Gain"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: gainSlider
                                    opacity: enabled ? 1 : 0.1
                                    width: 110
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: gainValue
                                    text: gainSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: gainSlider.minimumValue; top: gainSlider.maximumValue;}
                                    opacity: 1
                                    style: econTextFieldStyle
                                    implicitWidth: 50
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            gainSlider.value = gainValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:10
                                Text {
                                    id: shutterSpeedText
                                    text: "Shutter Speed"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                    wrapMode: Text.WordWrap
                                }
                                TextField {
                                    id: shutterSpeedTextField1
                                    text: "[1 - 1000000]"
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    style: econTextFieldStyle
                                    implicitWidth: 80
                                    onTextChanged: {

                                    }
                                }
                                TextField {
                                    id: shutterSpeedTextField2
                                    text:"[1 - 1000000]"
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    style: econTextFieldStyle
                                    implicitWidth: 80
                                    onTextChanged: {

                                    }
                                }

                            }
                            Row{
                                Text{
                                    id: ispWDRSettings
                                    text: "--- ISP Wide Dynamic Range Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }

                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: levelText
                                    text: "Level"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: levelCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "Disable"  }
                                        ListElement { text: "level1" }
                                        ListElement { text: "level2" }
                                        ListElement { text: "level3" }
                                        ListElement { text: "Customize" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: levelComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: levelComboSize.sourceSize.width - 25
                                                height: levelComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 12
                                Text {
                                    id: customLevelText
                                    text: "Custom Level"
                                    width: 70
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                    wrapMode: Text.WordWrap
                                }
                                TextField {
                                    id: customLevelTextField
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    style: econTextFieldStyle
                                    implicitWidth: 145
                                    onTextChanged: {

                                    }
                                }

                            }
                            Row{
                                Text{
                                    id: ispWBSettings
                                    text: "     --- ISP WhiteBalance Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }

                            }
                            Row{
                                Text {
                                    id: wbFullFrameText
                                    text: "WB Window:"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }

                            Row{
                                spacing: 50
                                ExclusiveGroup { id: wbRadio }
                                RadioButton {
                                    exclusiveGroup: wbRadio
                                    id: wbFullFrameRadio
                                    text: "Full Frame"
                                    activeFocusOnPress: true
                                    style: radioButtonWithoutWordWrapStyle

                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }
                                RadioButton {
                                    exclusiveGroup: wbRadio
                                    id: wbCustomizedRadio
                                    text: "Customized"
                                    activeFocusOnPress: true
                                    style: radioButtonWithoutWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }
                            }
                            Row{
                                Text {
                                    id: wbGrid
                                    text: "Region Of Interest:"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }
                            Row{
                                Grid{
                                    rows: 15
                                    columns: 16
                                    Repeater{
                                        model: (15*16)
                                        CheckBox {
                                            id: wb_roicheckboxGrid
                                            style: CheckBoxStyle {
                                                label: Text {
                                                    id: wb_roigrid
                                                    font.pixelSize: 14
                                                    font.family: "Ubuntu"
                                                    color: "#ffffff"
                                                    smooth: true
                                                    opacity: 1
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: wbmodeText
                                    text: "Wb mode"
                                    width: 80
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }

                                ComboBox {
                                    id: wbmodeCombo
                                    opacity: 1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "Auto"  }
                                        ListElement { text: "Simple" }
                                        ListElement { text: "Manual" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: wbmodeComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: wbmodeComboSize.sourceSize.width - 25
                                                height: wbmodeComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 12
                                Text {
                                    id: redGainText
                                    text: "Red Gain"
                                    width: 80
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                TextField {
                                    id: redGainTextField
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    style: econTextFieldStyle
                                    implicitWidth: 145
                                    onTextChanged: {

                                    }
                                }

                            }
                            Row{
                                spacing: 12
                                Text {
                                    id: blueGainText
                                    text: "Blue Gain"
                                    width: 80
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                TextField {
                                    id: blueGainTextField
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    style: econTextFieldStyle
                                    implicitWidth: 145
                                    onTextChanged: {

                                    }
                                }

                            }
                            Row{
                                Text{
                                    id: ispIASettings
                                    text: "  --- ISP Image Adjustment Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }

                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: brightnessText
                                    text: "Brightness"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: brightnessSlider
                                    opacity: enabled ? 1 : 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: brightnessValue
                                    text: brightnessSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: brightnessSlider.minimumValue; top: brightnessSlider.maximumValue;}
                                    opacity: 1
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            brightnessSlider.value = brightnessValue.text
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: contrastText
                                    text: "Contrast"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: contrastSlider
                                    opacity: enabled ? 1 : 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: contrastValue
                                    text: contrastSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: contrastSlider.minimumValue; top: contrastSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            contrastSlider.value = contrastValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:15
                                Text {
                                    id: saturationText
                                    text: "Saturation"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: saturationSlider
                                    opacity: enabled ? 1 : 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: saturationValue
                                    text: saturationSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: saturationSlider.minimumValue; top: saturationSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            saturationSlider.value = saturationValue.text
                                    }
                                }
                            }
                            Row{
                                Text{
                                    id: ispNoiseReductionSettings
                                    text: "  --- ISP Noise Reduction Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }
                            }
                            Row{
                                CheckBox {
                                    id: deImpulse
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: deImpulseText
                                            text: "De-Impulse"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                        }
                                    }
                                }
                            }
                            Row{
                                Text{
                                    id: preNRCtrlText
                                    text: "Pre-NR Control:"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }
                            Row{
                                CheckBox {
                                    id: preNR
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: preNRCheckText
                                            text: "Pre-NR"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                        }
                                    }
                                }
                            }
                            Row{
                                spacing:15
                                Text {
                                    id: preNRstrength
                                    text: "Strength"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: preNR.checked ? 1: 0.1
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: preNRSlider
                                    enabled: preNR.checked ? true : false
                                    opacity: enabled ? 1 : 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: preNRValue
                                    text: preNRSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    enabled: preNR.checked ? true : false
                                    opacity: enabled ? 1 : 0
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: preNRSlider.minimumValue; top: preNRSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            preNRSlider.value = preNRValue.text
                                    }
                                }

                            }
                            Row{
                                Text{
                                    id: twod3DCtrlText
                                    text: "2D/3D - NR Controls:"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 1
                                }
                            }
                            Row{
                                CheckBox {
                                    id: twod3DNRCheck
                                    style: CheckBoxStyle {
                                        label: Text {
                                            id: twod3DNRCheckText
                                            text: "2D/3D - NR"
                                            font.pixelSize: 14
                                            font.family: "Ubuntu"
                                            color: "#ffffff"
                                            smooth: true
                                            opacity: 1
                                        }
                                    }
                                }
                            }

                            Row{
                                spacing: 15
                                Text {
                                    id: twod3DNRMode
                                    text: "NR Mode"
                                    width: 80
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: twod3DNRCheck.checked ? 1 : 0.1
                                }

                                ComboBox {
                                    id: twod3DNRmodeCombo
                                    enabled: twod3DNRCheck.checked ? true : false
                                    opacity: enabled ? 1 : 0.1
                                    //model: resolutionModel
                                    //textRole: "display"
                                    model: ListModel {
                                        ListElement { text: "xxx"  }
                                        ListElement { text: "yyy" }
                                        ListElement { text: "zzzz" }
                                    }
                                    smooth: true
                                    activeFocusOnPress: true
                                    style: ComboBoxStyle {
                                        background: Image {
                                            id: twod3DNRmodeComboSize
                                            source: "images/wificam_combobox.png"
                                            Rectangle {
                                                width: twod3DNRmodeComboSize.sourceSize.width - 25
                                                height: twod3DNRmodeComboSize.sourceSize.height + 3
                                                color: "#222021"
                                                border.color: "white"
                                                border.width: control.activeFocus ? 3 : 1
                                                radius: control.activeFocus ? 5 : 0
                                            }
                                        }
                                        label: Text {
                                            anchors.fill: parent
                                            color: "#ffffff"
                                            text: control.currentText
                                            elide: Text.ElideRight
                                            verticalAlignment: Text.AlignVCenter
                                            font.family: "Ubuntu"
                                            font.pixelSize: 14
                                        }
                                    }
                                    onCurrentIndexChanged: {
                                    }
                                    Component.onCompleted: {
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: normalStrengthText
                                    text: "Normal Strength"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: normalStrengthSlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: normalStrengthValue
                                    text: normalStrengthSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: normalStrengthSlider.minimumValue; top: normalStrengthSlider.maximumValue;}
                                    opacity: 0.1
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            normalStrengthSlider.value = normalStrengthValue.text
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: refStrengthText
                                    text: "Reference Strength"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: refStrengthSlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: refStrengthValue
                                    opacity: 0.1
                                    text: refStrengthSlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: refStrengthSlider.minimumValue; top: refStrengthSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            refStrengthSlider.value = refStrengthValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:15
                                Text {
                                    id: currentWeightText
                                    text: "Current Weight"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: currentWeightSlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: currentWeightValue
                                    text: currentWeightSlider.value
                                    opacity: 0.1
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: currentWeightSlider.minimumValue; top: currentWeightSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            currentWeightSlider.value = currentWeightValue.text
                                    }
                                }
                            }
                            Row{
                                spacing: 20
                                Text {
                                    id: motionAdaptiveText
                                    text: "Motion Adaptive"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    width: 60
                                    wrapMode: Text.WordWrap
                                }

                                ExclusiveGroup { id: motionAdaptiveRadio }
                                RadioButton {
                                    exclusiveGroup: motionAdaptiveRadio
                                    id: motionAdaptiveRadioDisable
                                    text: "Disable"
                                    opacity: 0.1
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked:{

                                    }
                                    onCheckedChanged: {

                                    }
                                    Keys.onReturnPressed: {
                                    }
                                }
                                RadioButton {
                                    exclusiveGroup: motionAdaptiveRadio
                                    id: motionAdaptiveRadioEnable
                                    text: "Enable"
                                    opacity: 0.1
                                    activeFocusOnPress: true
                                    style: radioButtonWordWrapStyle
                                    onClicked: {

                                    }

                                    Keys.onReturnPressed: {

                                    }
                                }
                            }
                            Row{
                                Text{
                                    id: ispEdgeEnhancementSettings
                                    text: "  --- ISP Edge Enhancement Settings ---"
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.50196078431373
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: edgeSensitivityText
                                    text: "Edge Sensitivity"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: edgeSensitivitySlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: edgeSensitivityValue
                                    text: edgeSensitivitySlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: edgeSensitivitySlider.minimumValue; top: edgeSensitivitySlider.maximumValue;}
                                    opacity: 0.1
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            edgeSensitivitySlider.value = edgeSensitivityValue.text
                                    }
                                }
                            }
                            Row{
                                spacing: 15
                                Text {
                                    id: noiseSensitivityText
                                    text: "Noise Sensitivity"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: noiseSensitivitySlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: noiseSensitivityValue
                                    opacity: 0.1
                                    text: noiseSensitivitySlider.value
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: noiseSensitivitySlider.minimumValue; top: noiseSensitivitySlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            noiseSensitivitySlider.value = noiseSensitivityValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:15
                                Text {
                                    id: edgeStrengthText
                                    text: "Edge Strength"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: edgeStrengthSlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: edgeStrengthValue
                                    text: edgeStrengthSlider.value
                                    opacity: 0.1
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: edgeStrengthSlider.minimumValue; top: edgeStrengthSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            edgeStrengthSlider.value = edgeStrengthValue.text
                                    }
                                }
                            }
                            Row{
                                spacing:15
                                Text {
                                    id: edgeStrengthClipText
                                    text: "Edge Strength Clip"
                                    width: 60
                                    font.pixelSize: 14
                                    font.family: "Ubuntu"
                                    color: "#ffffff"
                                    smooth: true
                                    opacity: 0.1
                                    wrapMode: Text.WordWrap
                                }
                                Slider {
                                    activeFocusOnPress: true
                                    updateValueWhileDragging: false
                                    id: edgeStrengthClipSlider
                                    opacity: 0.1
                                    width: 100
                                    stepSize: 1
                                    style:econSliderStyle
                                    onValueChanged:  {
                                    }
                                }
                                TextField {
                                    id: edgeStrengthClipValue
                                    text: edgeStrengthClipSlider.value
                                    opacity: 0.1
                                    font.pixelSize: 10
                                    font.family: "Ubuntu"
                                    smooth: true
                                    horizontalAlignment: TextInput.AlignHCenter
                                    validator: IntValidator {bottom: edgeStrengthClipSlider.minimumValue; top: edgeStrengthClipSlider.maximumValue;}
                                    style: econTextFieldStyle
                                    implicitWidth: 45
                                    implicitHeight: 20
                                    onTextChanged: {
                                        if(text != "")
                                            edgeStrengthClipSlider.value = edgeStrengthClipValue.text
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    Component {
        id: radioButtonWithoutWordWrapStyle
        RadioButtonStyle {
            label: Text {
                width:50
                text: control.text
                font.pixelSize: 14
                font.family: "Ubuntu"
                color: "#ffffff"
                smooth: true
                opacity: 1
            }
            background: Rectangle {
                color: "#222021"
                border.color: control.activeFocus ? "#ffffff" : "#222021"
            }
        }
    }

    Component {
        id: radioButtonWordWrapStyle
        RadioButtonStyle {
            label: Text {
                width: 50
                text: control.text
                font.pixelSize: 14
                font.family: "Ubuntu"
                color: "#ffffff"
                smooth: true
                opacity: 1
                wrapMode: Text.WordWrap
            }
            background: Rectangle {
                color: "#222021"
                border.color: control.activeFocus ? "#ffffff" : "#222021"
            }
        }
    }

    Component {
        id: econButtonStyle
        ButtonStyle {
            background: Rectangle {
                implicitHeight: 30
                implicitWidth: 104
                border.width: control.activeFocus ? 3 :0
                color: "#e76943"
                border.color: control.activeFocus ? "#ffffff" : "#222021"
                radius: control.activeFocus ? 5 : 0
            }
            label: Text {
                color: "#ffffff"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.family: "Ubuntu"
                font.pointSize: 10
                text: control.text
            }
        }
    }

    Component {
        id: econTextFieldStyle
        TextFieldStyle {
            textColor: "black"
            background: Rectangle {
                radius: 2
                border.color: "#333"
                border.width: 2
                y: 1
            }
        }
    }
    // make wifi interface config controls visible
    function wifiInterfaceConfiguration(){
        if(!wifiIfaceConfigScroll.visible){
            wifiIfaceConfigScroll.visible = true
        }
    }

    // make wifi camera config controls visible
    function wifiCameraConfiguration(){
        if(!wifiCameraConfigScroll.visible){
            wifiCameraConfigScroll.visible = true
        }
    }


    WifiCam{
        id: wificamera
        onDeviceStatus:{
            messageDialog.title = title.toString()
            messageDialog.text = message.toString()
            messageDialog.open()
        }
    }
    Component.onDestruction:{
        wificamera.socketClose()
    }
}
