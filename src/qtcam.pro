# Add more folders to ship with the application, here
folder_01.source = qml/qtcam
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QT += widgets opengl multimedia xml
TARGET = Qtcam

CONFIG += release

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    logger.cpp \
    about.cpp \
    cameraproperty.cpp \
    videostreaming.cpp \
    uvccamera.cpp \
    v4l2-api.cpp \
    seecam_10cug_m.cpp \
    seecam_10cug_bayer.cpp \
    seecam_11cug.cpp \
    seecam_cu80.cpp \
    seecam_cu50.cpp \
    seecam_ar0130.cpp \
    videoencoder.cpp \
    seecam_cu51.cpp \
    see3cam_cu130.cpp \
    h264decoder.cpp \
    ascella.cpp \
    wificam.cpp
    #wpa_command.c


# Installation path
# target.path =
target.path = /usr/

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()


HEADERS += \
    logger.h \
    about.h \
    cameraproperty.h \
    videostreaming.h \
    uvccamera.h \
    v4l2-api.h \
    seecam_10cug_m.h \
    seecam_10cug_bayer.h \
    seecam_11cug.h \
    seecam_cu80.h \
    seecam_cu50.h \
    seecam_ar0130.h \
    videoencoder.h \
    seecam_cu51.h \
    see3cam_cu130.h \
    h264decoder.h \
    ascella.h \
    wificam.h \
    wpa/os.h \
    wpa/includes.h \
    wpa/linkedlist.h \
    wpa/wpa_command.h \
    vlcrtspmedia.h


INCLUDEPATH +=  /usr/include \
                $$PWD/wpa/include \
                $$PWD/v4l2headers/include \               
                /usr/include/libusb-1.0 \
                /usr/include/GL/


LIBS += -lv4l2 -lv4lconvert \
        -lavutil \
        -lavcodec \
        -lavformat \
        -lswscale \
        -ludev \
        -lusb-1.0 \
        -L/home/kaushik/sankari_project/wifi_connect_lib_creation/build-wificonnect-Desktop_Qt_5_2_1_GCC_64bit-Debug/ -lwificonnect \
        -L/usr/lib/ -lturbojpeg -lvlc\
        -L/lib/ -lGLEW \
        -L/home/kaushik/sankari_project/vlc_rtsp_lib/build-vlcrtsp-Desktop_Qt_5_2_1_GCC_64bit-Debug/ -lvlcrtsp \
        -L/home/kaushik/sankari_project/doc/ewificam_app/lib -lnetcam64


QMAKE_CFLAGS_THREAD = -D__STDC_CONSTANT_MACROS       #For Ubuntu 12.04 compilation
QMAKE_CXXFLAGS_THREAD = -D__STDC_CONSTANT_MACROS   #For Ubuntu 12.04 compilation

OTHER_FILES += \
    qml/qtcam/videocapturefilter_QML/videocapturefilter_qml.qml
