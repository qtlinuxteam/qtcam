/*
 * wificam.cpp -- wifi network scan.
 * Copyright © 2015  e-con Systems India Pvt. Limited
 *
 * This file is part of Qtcam.
 *
 * Qtcam is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Qtcam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qtcam. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QDebug>
#include <string.h>
#include "wificam.h"


extern "C"{
#include "wpa/wpa_command.h"
#include "wpa/linkedlist.h"
}

WifiCam::WifiCam():m_ssidList(NULL),
                   m_ctrlInterfaceName(NULL){

}

WifiCam::~WifiCam(){

}

/**
 * @brief get default wlan interface name
 * @param NIL
 * @returns true/false
 */
bool WifiCam::getDefaultInterfaceName(){
    if(!m_ctrlInterfaceName){
        // Get the default wireless interface name
        m_ctrlInterfaceName = wpa_cli_get_default_ifname();

        // if interface name [ex:wlan0] is NULL, failure
        if(!m_ctrlInterfaceName)
            return false;
    }

    return true;
}

/**
 * @brief open wifi connection using interface name
 * @param NIL
 * @returns true/false
 */
bool WifiCam::openConnection(){
    if(!m_ctrlInterfaceName)
        return false;

    // opening wifi interface connection
    if(wpa_cli_open_connection(m_ctrlInterfaceName) != 0){
        return false;
    }

    return true;
}

/**
 * @brief scan all APs and extract wifi camera from AP list
 * @param NIL
 * @returns wifi camera list
 */
QStringList WifiCam::extractWifiCamsFromAPList(){
     QStringList wifiCamList;

     // sending scan and scan_results commands.
     // scan results gives list in the format "bssid / frequency / signal level / flags / ssid"
     // if failure to get the above list, return empty list
     if (0 != scan_ssid())
         return wifiCamList;

     //extract ssid names from the format above mentioned
     m_ssidList = get_ssid_list();
     while(m_ssidList != NULL)
     {         
         // extracting wifi cameras from all wifi access points
         if(strstr(m_ssidList->ssid, "Cam") || strstr(m_ssidList->ssid, "cam"))
            wifiCamList.append(m_ssidList->ssid);
         m_ssidList = m_ssidList->next;
     }

     return wifiCamList;
}

/**
 * @brief: close wifi interface connection
 * @param NIL
 * @returns NIL
 */
void WifiCam::closeConnection(){    
    wpa_cli_close_connection();
}

/**
 * @brief: Getting status of selected camera status
 * @param: device name
 * @returns true/false
 */
bool WifiCam::getStatus(QString deviceName){

    // find status of selected camera device
    if(0 != find_status()){
        return false;
    }

    // if "wpa_state is COMPLETED" then we can conclude as wifi camera is connected.
    if(strstr(statusCmdBuf,deviceName.toStdString().c_str()) && strstr(statusCmdBuf, "wpa_state=COMPLETED")){           
        return true;
    }
    else{                
        return false;
    }
}

/**
 * @brief WifiCam::socketInit - Initializing the socket connection for wifi camera
 * @param interface name
 * @param ip address
 */
bool WifiCam::socketInit(QString ifname, QString ipaddr){
    QByteArray ifNameBA = ifname.toLocal8Bit();
    char* ifName = ifNameBA.data();

    QByteArray ipaddrBA = ipaddr.toLocal8Bit();
    char* ipAddr = ipaddrBA.data();

    m_wifiCamhandle = NULL;
    m_wifiCamhandle = icam_socket_init(ifName, ipAddr);

    if(!m_wifiCamhandle){
        emit deviceStatus("Error", "wificam socket initialization is failed");
    }
    else{
        qDebug()<<"socket initialization is success";
    }
    return true;
}

/**
 * @brief WifiCam::socketClose - Closing the socket connection after use
 */
void WifiCam::socketClose(){
    if(m_wifiCamhandle){
        icam_socket_close(m_wifiCamhandle);
    }
}


/**
 * @brief WifiCam::camQueryCapability - Query the camera capability buffer[in the format of xml]
 */
void WifiCam::camQueryCapability(){

    if(!m_wifiCamhandle){
        // need to add in log
        emit deviceStatus("Error", "Cannot query camera capability");
        return;
    }

    int capbufferSize = (20 * 1024);
    m_camCapability = NULL;
    m_camCapability = (unsigned char *) malloc(capbufferSize);
    if (!m_camCapability) {
        fprintf(stderr, "inet_query_capability: failed to allocate memory for query capability\n");
        return;
    }

    memset((void *) m_camCapability, 0, capbufferSize);
    icam_query_capability(m_wifiCamhandle, m_camCapability, capbufferSize);

    saveCameraConfigXmlFile();

    free(m_camCapability);
}

/**
 * @brief WifiCam::saveCameraConfigXmlFile - Save the camera configuration xml file
 */
void WifiCam::saveCameraConfigXmlFile(){
    // create wifi camera config xml
    QString filename = "wifiCameraConfig.xml";
    QFileInfo checkFile(filename);
        // check if file exists
    if (checkFile.exists() && checkFile.isFile())
        return;

    QFile file(filename);
    file.open(QIODevice::WriteOnly);

    // convert camera capability buffer to QString
    QString camCapabilityQstr((const char *)m_camCapability);

    // xml reader
    QXmlStreamReader reader(camCapabilityQstr);

    // xml writer
    QXmlStreamWriter writer(&file);

    // enable auto formating in xml writer
    writer.setAutoFormatting(true);

    // read the buffer and write into a file
    while (!reader.atEnd()) {
        reader.readNext();
        if (!reader.isWhitespace()) {
            writer.writeCurrentToken(reader);
        }
    }
    file.close();
}

void WifiCam::parseCameraConfigXmlFile(){
    // open the camera config xml file
    openCameraConfigXmlFile();

    //parse video setting Elements
    parseVideoSettingElements();

}

void WifiCam::openCameraConfigXmlFile(){
    // open wifi camera config file
    QString fileName("wifiCameraConfig.xml");
    xmlFile.setFileName(fileName);
    xmlFile.open(QIODevice::ReadOnly);
    //xml.setDevice(&xmlFile);
}

void WifiCam::parseVideoSettingElements(){
    /* parse video settings */
    // xml reader
    bool attrNameAvailable = false;
    bool attrTypeAvailable = false;
    bool attributeTypegrid = false;

    int i=0;

    QXmlStreamReader reader(&xmlFile);

    while (!reader.atEnd() && !reader.hasError())
    {
        reader.readNext();
        if (reader.isStartElement())
        {
            foreach(const QXmlStreamAttribute &attr, reader.attributes()) {
                if (attr.name().toString() == QLatin1String("name")) {
                    QString attribute_value = attr.value().toString();
                    qDebug()<<"attribute value"<<attribute_value;
                    attrNameAvailable = true;

                }
                if (attr.name().toString() == QLatin1String("type")) {
                    QString attribute_value = attr.value().toString();
                    qDebug()<<"attribute value"<<attribute_value;
                    attrTypeAvailable = true;
                    if(attribute_value == "grid")
                        attributeTypegrid = true;
                }

            }
            if(attrNameAvailable && attrTypeAvailable){

                reader.readNextStartElement();
                camConfigVal[i].support = reader.readElementText();
                qDebug()<<camConfigVal[i].support;

                if(attributeTypegrid){
                    for(int j=0; j<=15; j++){
                        reader.readNextStartElement();
                        camConfigVal[i].defaultValue.append(reader.readElementText());
                        camConfigVal[i].defaultValue.append(",");
                        qDebug()<<camConfigVal[i].defaultValue;
                    }
                }
                else{
                    reader.readNextStartElement();
                    camConfigVal[i].defaultValue = reader.readElementText();
                    qDebug()<<camConfigVal[i].defaultValue;
                }

                if(attributeTypegrid){
                    for(int j=0; j<=15; j++){
                        reader.readNextStartElement();
                        camConfigVal[i].currentValue.append(reader.readElementText());
                        camConfigVal[i].currentValue.append(",");
                        qDebug()<<camConfigVal[i].currentValue;
                    }
                }
                else{
                    reader.readNextStartElement();
                    camConfigVal[i].currentValue = reader.readElementText();
                    qDebug()<<camConfigVal[i].currentValue;
                }
                i++;
            }
            else{
                qDebug()<<"type is not available";
            }
            attrNameAvailable = false;
            attrTypeAvailable = false;
            attributeTypegrid = false;
        }
    }
    if (reader.hasError())
    {
        qDebug() << "XML error: " << reader.errorString() << endl;
    }
    else if (reader.atEnd())
    {
        qDebug() << "Reached end, done" << endl;
    }
}
